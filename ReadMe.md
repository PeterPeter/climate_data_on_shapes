# Installation

This project requires python3 with the following packages:  
- xesmf
- numpy
- scipy
- netCDF4
- matplotlib
- shapely
- pandas
- fiona
- cartopy
- xarray

class_on_shape.py is the main class

There is an example application: example/example_Benin.ipynb
